﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace lessons.lesson02
{
    public class Lesson02
    {
        public void Run()
        {
            FunWithStrings();
        }

        #region Type conversions

        static int Add(int x, int y)
        {
            return x + y;
        }

        static void TypeConversions2()
        {
            for (int index = 0; index < 1000000; index++)
            {
                decimal d = 10.0m + index * 0.000001m;
                int i = (int)d;

                Console.WriteLine($"double {d} => int {i}");
            }
        }

        static void TypeConversions()
        {
            int a = 30;
            double b = a; //30.0

            Console.WriteLine("{0}", b);
        }

        static void OverflowExample()
        {
            Console.WriteLine("***** Fun with type conversions *****\n");
            short numb1 = 30000, numb2 = 30000;

            // Explicitly cast the int into a short (and allow loss of data).
            short answer = (short)Add(numb1, numb2);

            Console.WriteLine("{0} + {1} = {2}",
                numb1, numb2, answer);
        }

        static void NarrowingAttempt()
        {
            byte myByte = 0;
            int myInt = 200;

            // Explicitly cast the int into a byte (no loss of data).
            myByte = (byte)myInt;
            Console.WriteLine("Value of myByte: {0}", myByte);
        }

        static void ProcessBytes()
        {
            byte b1 = 100;
            byte b2 = 250;

            // This time, tell the compiler to add CIL code
            // to throw an exception if overflow/underflow
            // takes place.
            byte sum = checked((byte)Add(b1, b2));
            Console.WriteLine($"sum = {sum}");
        }

        static void NarrowWithConvert()
        {
            byte myByte = 0;
            int myInt = 200;
            myByte = Convert.ToByte(myInt);
            Console.WriteLine("Value of myByte: {0}", myByte);
        }

        #endregion

        #region Strings

        static void StringFormats()
        {
            string stringFormat = "My name if: {0}";
            string myName = "Vasja";

            string result1 = string.Format(stringFormat, myName);

            Console.WriteLine(result1);

            //---------------------------------------------------------------------------------------

            string strFormat2 = "My name is {0}, and surname is {1}";
            string mySurname = "Pupkin";

            string result2 = string.Format(strFormat2, myName, mySurname);

            Console.WriteLine(result2);

            //---------------------------------------------------------------------------------------

            Console.WriteLine(string.Format("My name is {0}, and surname is {1}", myName, mySurname));

            //---------------------------------------------------------------------------------------

            Console.WriteLine($"My name is {myName}, and surname is {mySurname}");

            //---------------------------------------------------------------------------------------

            int a = 10;
            bool b = true;
            double c = 10.5;
            float d = 10.5f;

            Console.WriteLine($"a = {a}, b = {b}, c = {c}, d = {d}");

            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);

            Console.WriteLine(a.ToString());
            Console.WriteLine(b.ToString());
            Console.WriteLine(c.ToString());
            Console.WriteLine(d.ToString());
        }

        static void FunWithStrings()
        {
            Console.WriteLine("Basic string functionality:");

            string firstName = "Denny";

            Console.WriteLine($"FirstName = {firstName}");
            Console.WriteLine($"FirstName has {firstName.Length} characters.");
            Console.WriteLine($"FirstName in uppercase = {firstName.ToUpper()}");
            Console.WriteLine($"FirstName in lowercase = {firstName.ToLower()}");

            Console.WriteLine($"FirstName contains 'y'?: {firstName.Contains('y')}");
            Console.WriteLine($"FirstName contains 'y'?: {firstName.Contains("y")}");

            Console.WriteLine($"FirstName after replace 'nn' to '__': {firstName.Replace("nn", "__")}");

            Console.WriteLine();
        }

        static void EscapeChars()
        {
            Console.WriteLine("\t\t\t Escape characters: \a");

            Console.WriteLine("Everyone loves \"Hello World!\"");
            Console.WriteLine("C:\\MyApp\\bin\\Debug");

            Console.WriteLine("All finished! \a\a \n New line:\n New line:\n New line:\n New line:\n New line:\n");

            //---------------------------------------------------------------------------------------

            Console.WriteLine("With @ characters:");

            Console.WriteLine(@"Everyone loves ""Hello World!""    \a\a");
            Console.WriteLine(@"C:\MyApp\bin\Debug");

            string longString = @"This is 
                                    long
                                        long
                                            long
                                                string";

            Console.WriteLine(longString);
        }

        static void StringEquality()
        {
            Console.WriteLine("Compare strings:");

            string s1 = "Hello";
            string s2 = "Yo!";

            Console.WriteLine($"\"Hello\": {s1}");
            Console.WriteLine($"\"Yo!\": {s2}");
            Console.WriteLine();

            Console.WriteLine($"s1 == s2: {s1 == s2}");
            Console.WriteLine($"s1 == Hello!: {s1 == "Hello!"}");
            Console.WriteLine($"s1 == hello!: {s1 == "hello!"}");
            Console.WriteLine($"s1.Equals(s2): {s1.Equals(s2)}");
            Console.WriteLine($"Yo!.Equals(s2): {"Yo!".Equals(s2)}");

            //---------------------------------------------------------------------------------------

            Console.WriteLine("Some literals magic:");

            int a = 12;
            bool b = true;

            Console.WriteLine(a.ToString());
            Console.WriteLine(b.ToString());

            Console.WriteLine(12.ToString());
            Console.WriteLine(true.ToString());
            Console.WriteLine(15.5.ToString());
        }

        static void StringsAreImmutable()
        {
            string str = "This is my string";

            Console.WriteLine($"str = {str}");

            string upperString = str.ToUpper();
            Console.WriteLine(upperString);

            Console.WriteLine($"str = {str}");
        }

        static void StringConcatenation()
        {
            Console.WriteLine("Let's + strings");

            string s1 = "first Part";
            string s2 = "second part";

            string result = string.Concat(s1, s2);

            Console.WriteLine(result);
        }

        static void ConcatStringsStupid()
        {
            string str = "******";
            string str2 = new string('*', 6);

            string result = "";

            for (int i = 0; i < 100000; i++)
            {
                result = result + str2;
            }

            Console.WriteLine(result);
        }

        static void ConcatStringsSmart()
        {
            string str = "******";
            string str2 = new string('*', 6);

            StringBuilder result = new StringBuilder();

            for (int i = 0; i < 100000; i++)
            {
                result.Append(str2);
            }

            Console.WriteLine(result);
        }

        static void GetElapsedTime()
        {
            Stopwatch stopwatch;

            stopwatch = Stopwatch.StartNew();

            ConcatStringsSmart();

            stopwatch.Stop();

            Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }

        #endregion

        #region Parsing data

        static void ParseFromStrings()
        {
            Console.WriteLine("=> Data type parsing:");
            bool b = bool.Parse("True");
            Console.WriteLine("Value of b: {0}", b);
            double d = double.Parse("99.884");
            Console.WriteLine("Value of d: {0}", d);
            int i = int.Parse("8");
            Console.WriteLine("Value of i: {0}", i);
            char c = Char.Parse("w");
            Console.WriteLine("Value of c: {0}", c);
            Console.WriteLine();
        }

        static void ParseFromStringsWithTryParse()
        {
            Console.WriteLine("=> Data type parsing with TryParse:");
            if (bool.TryParse("True", out bool b))
            {
                Console.WriteLine("Value of b: {0}", b);
            }
            string value = "Hello";
            if (double.TryParse(value, out double d))
            {
                Console.WriteLine("Value of d: {0}", d);
            }
            else
            {
                Console.WriteLine("Failed to convert the input ({0}) to a double", value);
            }
            Console.WriteLine();
        }

        #endregion

        #region Implicit data typing

        static void DeclareImplicitVars()
        {
            // Implicitly typed local variables.
            var myInt = 0;
            var myBool = true;
            var myString = "Time, marches on...";

            // Print out the underlying type.
            Console.WriteLine("myInt is a: {0}", myInt.GetType().Name);
            Console.WriteLine("myBool is a: {0}", myBool.GetType().Name);
            Console.WriteLine("myString is a: {0}", myString.GetType().Name);
        }

        static int GetAnInt()
        {
            var retVal = 9;
            return retVal;
        }

        static void ImplicitTypingIsStrongTyping()
        {
            // The compiler knows "s" is a System.String.
            var s = "This variable can only hold string data!";
            s = "This is fine...";

            // Can invoke any member of the underlying type.
            string upper = s.ToUpper();

            // Error! Can't assign numerical data to a a string!
            // s = 44;
        }

        #endregion

        #region LINQ example

        static void QueryOverInts()
        {
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
            var subset = from i in numbers where i < 10 select i;

            Console.Write("Values in subset: ");
            foreach (var i in subset)
            {
                Console.Write("{0} ", i);
            }

            Console.WriteLine();

            // Hmm...what type is subset?
            Console.WriteLine("subset is a: {0}", subset.GetType().Name);
            Console.WriteLine("subset is defined in: {0}", subset.GetType().Namespace);
        }

        #endregion

        #region Bad use of var!

        // Uncomment to see compile errors.
        //class ThisWillNeverCompile
        //{
        //    // Error! var cannot be used as field data!
        //    private var myInt = 10;

        //    // Error! var cannot be used as a return value
        //    // or parameter type!
        //    public var MyMethod(var x, var y) { }
        //}

        #endregion
    }
}
