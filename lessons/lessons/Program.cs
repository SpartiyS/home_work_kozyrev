﻿using lessons.lesson04;

namespace main_namespace
{
    class Program
    {
        static void Main()
        {
            HomeWork04 lesson = new HomeWork04();

            lesson.Run();
        }
    }
}
