﻿using System;

namespace lessons.lesson03
{
    public class Lesson03
    {
        public void Run()
        {
            //FunWithIfElse();

            FunWithSwitch2();
        }

        #region ariphmetics

        static void FunWithAriphmetics()
        {
            int a = 0;
            a = a + 10;
            a += 10;

            // тоже самое для +, -, *, /
            int b = 0;
            b = b * 10;
            b *= 10;


            int c = 10;
            c = c % 4;
            //c %= 4;
            Console.WriteLine(c);
        }

        static void FunWithDecrement()
        {
            int a = 3;
            int b = 5;
            int c = 40;
            int d = c-- - b * a;    // a=3  b=5  c=39  d=25

            //int d = (c--)-(b*a);

            //int d = (c-(--b))*a;    // a=3  b=4  c=40  d=108

            Console.WriteLine($"a={a}  b={b}  c={c}  d={d}");


        }

        #endregion

        #region Simple Array
        static void SimpleArrays()
        {
            Console.WriteLine("=> Simple Array Creation.");
            // Create and fill an array of 3 Integers
            int[] myInts = new int[3];
            myInts[0] = 100;
            myInts[1] = 200;
            myInts[2] = 300;

            // Now print each value.
            foreach (int i in myInts)
                Console.WriteLine(i);
            Console.WriteLine();
        }
        #endregion

        #region Array Init Syntax
        static void ArrayInitialization()
        {
            Console.WriteLine("=> Array Initialization.");

            // Array initialization syntax using the new keyword.
            string[] stringArray = new string[] { "one", "two", "three" };
            Console.WriteLine("stringArray has {0} elements", stringArray.Length);

            // Array initialization syntax without using the new keyword.
            bool[] boolArray = { false, false, true };
            Console.WriteLine("boolArray has {0} elements", boolArray.Length);

            // Array initialization with new keyword and size.
            int[] intArray = new int[4] { 20, 22, 23, 0 };
            Console.WriteLine("intArray has {0} elements", intArray.Length);
            Console.WriteLine();
        }
        #endregion

        #region Var keyword with arrays
        static void DeclareImplicitArrays()
        {
            Console.WriteLine("=> Implicit Array Initialization.");

            // a is really int[].
            var a = new[] { 1, 10, 100, 1000 };
            Console.WriteLine("a is a: {0}", a.ToString());

            // b is really double[].
            var b = new[] { 1, 1.5, 2, 2.5 };
            Console.WriteLine("b is a: {0}", b.ToString());

            // c is really string[].
            var c = new[] { "hello", null, "world" };
            Console.WriteLine("c is a: {0}", c.ToString());
            Console.WriteLine();
        }
        #endregion

        #region Array of objects
        static void ArrayOfObjects()
        {
            Console.WriteLine("=> Array of Objects.");

            // An array of objects can be anything at all.
            object[] myObjects = new object[4];
            myObjects[0] = 10;
            myObjects[1] = false;
            myObjects[2] = new DateTime(1969, 3, 24);
            myObjects[3] = "Form & Void";

            foreach (object obj in myObjects)
            {
                // Print the type and value for each item in array.
                Console.WriteLine("Type: {0}, Value: {1}", obj.GetType(), obj);
            }
            Console.WriteLine();
        }
        #endregion

        #region if / else / switch

        static void FunWithIfElse()
        {
            int value = Console.Read();
            bool isPositive = value > 0;

            if (isPositive)
            {
                Console.WriteLine("Value is positive.");
            }
            else
            {
                Console.WriteLine("Value is negative.");
            }
        }

        static void FunWithSwitch()
        {
            string str = Console.ReadLine();
            string template = "You entered: \"{0}\".";

            switch (str)
            {
                case "a":
                    Console.WriteLine(string.Format(template, str));
                    break;
                case "b":
                    Console.WriteLine(string.Format(template, str));
                    break;
                case "c":
                    Console.WriteLine(string.Format(template, str));
                    break;
                case "d":
                    Console.WriteLine(string.Format(template, str));
                    break;
                case "e":
                    Console.WriteLine(string.Format(template, str));
                    break;

                default:
                    Console.WriteLine("I DONT KNOW THIS SYMBOL");
                    break;
            }
        }

        internal enum Color
        {
            Black = 0,
            White = 1,
            Red = 2,
            Cyan = 3,
            Brown = 4,
            Green = 5,
            Orange = 6
        }

        static void FunWithSwitch2()
        {
            Console.WriteLine("Enter number for choose your color:");
            Console.WriteLine("1 - Black");
            Console.WriteLine("2 - White");
            Console.WriteLine("3 - Red");
            Console.WriteLine("4 - Cyan");
            Console.WriteLine("5 - Green");
            Console.WriteLine("6 - Orange");
            Console.WriteLine();

            string inputString = Console.ReadLine();
            int colorNumber = Convert.ToInt32(inputString);

            Color color = (Color)colorNumber;

            switch(color)
            {
                case Color.Black:
                    Console.WriteLine("Black");
                    break;
                case Color.White:
                    Console.WriteLine("White");
                    break;
                case Color.Red:
                    Console.WriteLine("Red");
                    break;
                case Color.Cyan:
                    Console.WriteLine("Cyan");
                    break;
                case Color.Green:
                    Console.WriteLine("Green");
                    break;
            }
        }

        #endregion
    }
}
