﻿using System;

namespace lessons.lesson04
{
    public class HomeWork04
    {
        public void Run()
        {
            // проверка правильности выполнения 1-го задания
            TicketsSoldTest(new[] { 25, 100 }, false);
            TicketsSoldTest(new[] { 50, 25, 100 }, false);
            TicketsSoldTest(new[] { 25, 50, 25, 100, 50, 25 }, true);
            TicketsSoldTest(new[] { 25, 25, 50, 25, 100 }, true);

            // проверка правильности выполнения 2-го задания
            RankFootbalPlayersTest(RankFootballPlayers(new[] { 10, 5, 7, 18, 6 }), new[] { 4, 1, 3, 5, 2 });
            RankFootbalPlayersTest(RankFootballPlayers(new[] { 1, 10, 7, 12, 3 }), new[] { 4, 2, 3, 5, 1 });
            RankFootbalPlayersTest(RankFootballPlayers(new[] { 15, 5, 1, 3, 11 }), new[] { 1, 5, 2, 4, 3 });
            RankFootbalPlayersTest(RankFootballPlayers(new[] { 3, 10, 4, 5, 16 }), new[] { 5, 2, 4, 3, 1 });

            Console.WriteLine(GetSumOfMaxElementsAtRows());
        }

        #region задание 1

        //  Задача:  Только вышел новый фильм "Джентельмены" Гая Ричи.Много людей стоят в очереди на билетом.
        //  У каждого из них есть купюра 100, 50 или 25 гривен.Билет на "Джентельмены" стоит 25 гривен...
        //  Вася решил подзаработать немного деньжат и продать каждому в очереди билет.
        //  Сможет ли Вася продать билет всем людям в очереди и дать сдачу, если у него изначально нет денег и он продает билеты строго по очереди?
        //  Функция должна вернуть true - если может, иначе - false
        //  Кол-во денег у каждого человека в очереди - это массив: [100, 25, 25]
        //  Например:
        //  Если в очереди у людей есть следующие купюры: [25, 25, 50] - то функция вернет true, Вася продает билет первому - у него есть 25, потом второму у него уже 2 купюры по 25 и третьему дает сдачу 25.
        //  Если в очереди у людей есть следующие купюры: [25, 100] - то функция вернет false, Вася продает билет первому - у него есть 25, а второму не может, у него нет сдачи 75
        //  Задача взята с codewars.com

        /// <summary>
        /// Функция возвращает сможет ли Вася продать билеты в очереди
        /// </summary>
        /// <param name="intArray">Массив с кол-вом денег у людей</param>
        /// <returns>True - если может, иначе false</returns>w
        /// 

        public static bool CanSellTickets(int[] peopleInLine)
        {
            int nomX1 = 0;
            int nomX2 = 0;
            int nomX3 = 0;
            
            for (int i = 0; i < peopleInLine.Length; i++)
            {
                if (peopleInLine[i] == 25)
                {
                    nomX1++;
                }
                else if (peopleInLine[i] == 50)
                {
                    nomX2++;
                }
                else if (peopleInLine[i] == 100)
                {
                    nomX3++;
                }

                if (peopleInLine[i] == 50 || peopleInLine[i] == 100)
                {
                    if (peopleInLine[i] == 50 && nomX1 == 2)
                    {
                        nomX1 -= 2;
                        nomX2++;
                    }
                    else if (peopleInLine[i] == 50 && nomX1 == 1)
                    {
                        nomX1--;
                        nomX2++;
                    }
                    else if (peopleInLine[i] == 100 && nomX1 == 4)
                    {
                        nomX1 -= 4;
                        nomX3++;
                    }
                    else if (peopleInLine[i] == 100 && nomX1 == 2 && nomX2 == 1)
                    {
                        nomX1 -= 2;
                        nomX2 -= 1;
                        nomX3++;
                    }
                    else if (peopleInLine[i] == 100 && nomX2 == 2)
                    {
                        nomX2 -= 2;
                        nomX3++;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }


        public static void TicketsSoldTest(int[] queue, bool realAnswer)
        {
            string result = CanSellTickets(queue) == realAnswer ? "Right!" : "Wrong!";

            System.Console.WriteLine(result);
        }

        #endregion

        #region задание 2

        //        Ваш тимлид поручил вам напиыыыысать функцию RankFootballPlayers которая бы ранжировала футболистов по количеству забитых голов.
        //      Это очень важная функция, которая в дальнейшем будет использоваться для вычисления трансферной стоимости футболистов в новой мобильной игре.
        //      На вход вам передается массив уникальных значений, каждый элемент которого это кол-во забитых голов.

        //  Например: [10, 15, 14, 18, 3]
        //
        //  Ваша задача вернуть массив где каждый элемент это "место" футболиста в исходном массиве, например:
        //  [4, 2, 3, 1, 5] // 18 - 1 место, 15 - 2 место, 14 - 3 место, 10 - 4 место, 3 - 5 место
        //  Задача взята с codewars.com


        /// <summary>
        /// Функция возвращает массив с "местом" футболиста в зависимости 
        /// от кол-ва забитых голов в исходном массиве
        /// </summary>
        /// <param name="intArray">Целочисленный массив</param>
        /// <returns>массив с "местом" в исходном массиве</returns>
        /// 
        public static int[] RankFootballPlayers(int[] playersGoals)
        {
            int x = 1;

            int[] scorePlayer = new int [playersGoals.Length];
            int[] PozitionArray = new int [playersGoals.Length];

            Array.Copy(playersGoals, 0, scorePlayer, 0, playersGoals.Length);
            Array.Copy(playersGoals, 0, PozitionArray, 0, playersGoals.Length);

            Array.Sort(scorePlayer);
            Array.Reverse(scorePlayer);


            for (int i = 0; i < playersGoals.Length; i++)
            {
                for (int z = 0; z < playersGoals.Length; z++)
                {
                    if (playersGoals[i] == scorePlayer[z])
                    {
                        PozitionArray[z] = x;
                        x++;
                    }
                }
            }

            return PozitionArray;

        }
    static void RankFootbalPlayersTest(int[] actual, int[] expected)
        {
            if (actual.Length != expected.Length)
                return;

            for (int i = 0; i < actual.Length; i++)
            {
                if (actual[i] != expected[i])
                {
                    System.Console.WriteLine("WRONG!");
                    return;
                }
            }

            System.Console.WriteLine("GOOD!");
        }

        #endregion

        #region задание 3

        //   Вам нужно заполнить двумерный прямоугольный массив 4 * 5
        //  Необходимо найти максимальный элемент в строке, а после суммировать максимальные элементы всех строк

        // Например есть массив:
        //  3,  18, 5,  47, 11
        //  2,  10, 14, 11, 12
        //  20, 2,  10, 7,  9
        //  10, 1,  13, 9,  10

        // Ответ: 47 + 14 + 20 + 13 = 94

        static int[,] GetArrayFromUserInput()
        {
            //new int[4, 5];
            int[,] EnterArray = {
                 { 3,  18, 5,  47, 11 },
                 { 2,  10, 14, 11, 12 },
                 { 20, 2,  10, 7,  9 },
                 { 10, 1,  13, 9,  10 },
            };
            Console.WriteLine("Enter array");


            //for (int i = 0; i < 4; i++)
            //{
            //    for (int j = 0; j < 5; j++)
            //    {
            //        Console.WriteLine($"Enter a{i}{j}:");
            //        EnterArray[i, j] = Convert.ToInt32(Console.ReadLine());  // если надо сделать ввод !
            //    }
            //}

            //use for loops to get values for each element:
            //Console.WriteLine("Enter a[0][1]: ");

            return EnterArray;
        }

        static int GetSumOfMaxElementsAtRows()
        {
            var EnterArray = GetArrayFromUserInput();
            int sum = 0;
            int[] maxArray = new int[4];

            // find sum

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (maxArray[i] < EnterArray[i,j])
                        maxArray[i] = EnterArray[i, j];
                }
            }
            for (int i = 0; i < 4; i++)
            {
                sum += maxArray[i];
            }
            return sum;
        }

        #endregion
    }
}
